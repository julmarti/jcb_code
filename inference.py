import copy
from itertools import combinations

from data_processing import filter_var

import numpy as np


def mal_inference(T, reaction, sp, cata=[], stochastic=False):
    """
    Compute mass action law parameter given transitions and index of chosen species to compute variations (proper to each reaction).

    Return mass action law parameter, relative standard deviation and number of transitions used in the inference.
    """
    reactants, products = list(np.where(reaction == -1)[0]), list(np.where(reaction == 1)[0])
    sgn = 1 if sp in products else -1
    W, s_involved = [], reactants + products
    reactants = reactants + cata
    count = 0
    for p in range(len(T)):
        # avoid division by zero and numerical discrepancies
        if (np.prod([T[p][0][r + 1] for r in reactants]) > 0) and (np.prod([T[p][1][s + 1] for s in s_involved])):
            count += 1
            # case x => _
            if not np.where(reaction == 1)[0].size:
                k = -T[p][1][sp + 1] / (T[p][1][0] * np.prod([T[p][0][r + 1] for r in reactants]))
            # case _ => x
            if not np.where(reaction == -1)[0].size:
                if not len(cata):
                    k = T[p][1][sp + 1] / T[p][1][0]
                else:
                    k = T[p][1][sp + 1] / (T[p][1][0] * T[p][0][cata[0] + 1])
            # case x => y
            else:
                k = sgn * T[p][1][sp + 1] / (T[p][1][0] * np.prod([T[p][0][r + 1] for r in reactants]))
        else:
            continue
        W.append(k**(-1))
    # stochastic complexation case
    if stochastic and len(reactants) > 1:
        W = [k / ((len(reactants) - 1) * 100) for k in W]
    if W == [] or (len(W) == 1):
        return 0, np.inf, 0
    avg_k = 1 / np.mean(W)
    return avg_k, np.std(W) * np.abs(avg_k), count


def hill_inference(T, reaction, sp):
    """
    Compute hill order 1 and 4 kinetics given transitions. Only for single reactant reactions.

    Return MM and Hill kinetics, relative standard deviations.
    """
    reactant, s_involved = np.where(reaction == -1)[0][0], np.where(reaction != 0)[0]
    W_1, W_4, gaps = [], [], []
    sgn = -1 if sp == reactant else 1
    # get index where species s is most present
    s_argmax = np.argmax([T[i][0][reactant + 1] if T[i][1][reactant + 1] != 0 else 0 for i in range(len(T))], axis=0)
    vmax = np.abs(T[s_argmax][1][reactant + 1] / T[s_argmax][1][0])
    # get index where species concentration is s.t. d_s/d_0 close to vmax/2
    for d in range(len(T)):
        gap = np.abs(1 - np.abs(((T[d][1][reactant + 1] / T[d][1][0]) / (vmax / 2))))
        gaps.append(gap)
    min_gap = np.argmin(gaps)
    Km = T[min_gap][0][reactant + 1]
    for p in range(len(T)):
        A = T[p][0][reactant + 1]
        # avoid numerical errors
        if (A > 0) and (np.prod([T[p][1][s + 1] for s in s_involved])):
            v_1 = (A / (A + Km)) * (T[p][1][0] / (sgn * (T[p][1][sp + 1])))
            v_4 = (A**4 / (A**4 + Km**4)) * (T[p][1][0] / (sgn * (T[p][1][sp + 1])))
        else:
            continue
        W_1.append(v_1), W_4.append(v_4)
    vmax_micha, vmax_hill = 1 / np.mean(W_1), 1 / np.mean(W_4)
    rho_micha, rho_hill = np.std(W_1) * np.abs(vmax_micha), np.std(W_4) * np.abs(vmax_hill)
    return vmax_micha, vmax_hill, Km, rho_micha, rho_hill


def catalyst_inference(reaction, T, header, alpha, micha, sp, stochastic=False, verbose=False):
    """Compute kinetics, catalyst if necessary and chose the best."""
    cata = []
    avg_k, rho, count = mal_inference(T, reaction, sp, cata=[], stochastic=stochastic)
    if count == 1:
        return np.inf, 0, cata, 'mal'
    if verbose:
        print('mean = {}\nrelative std = {}\n'.format(avg_k, rho))
    if (rho < alpha):
        return rho, np.round(avg_k, 4), cata, 'mal'
    else:
        # trying mm/hill kinetics :
        if micha and (np.where(reaction == -1)[0]).size == 1:
            vmax_micha, vmax_hill, km, rho_micha, rho_hill = hill_inference(T, reaction, sp)
            hills = [rho_micha, rho_hill]
            if verbose:
                print('michaelis case\nvmax = {}\n km = {}\nrelative std = {}\n'.format(vmax_micha, km, rho_micha))
                print('hill 4 case\nvmax = {}\n km = {}\nrelative std = {}\n'.format(vmax_hill, km, rho_hill))
            if min(hills) < alpha:
                if np.argmin(hills) == 0:
                    return min(hills), [vmax_micha, km, 1], cata, 'mm'
                else:
                    return min(hills), [vmax_hill, km, 4], cata, 'hill_4'
        # trying to find a catalyst
        n_species = len(T[0][0]) - 1
        rho_S, avg_k_S = np.ones(n_species) * np.inf, np.zeros(n_species)
        for s in range(len(reaction)):
            if verbose:
                print('trying species {}\n'.format(header[s + 1]))
            avg_k_S[s], rho_S[s], count = mal_inference(T, reaction, sp, cata=[s], stochastic=stochastic)
            if count == 1:
                return rho, np.round(avg_k, 4), cata, 'mal'
            if verbose:
                print('mean = {}\nrelative std = {}\n'.format(avg_k_S[s], rho_S[s]))
        if np.min(rho_S) < alpha:
            if verbose:
                print('{} catalyst\n'.format(header[np.argmin(rho_S) + 1]))
            cata = np.argmin(rho_S)
            return np.min(rho_S), np.round(avg_k_S[np.argmin(rho_S)], 4), cata, 'mal'
        else:
            if verbose:
                print('failed to found catalyst\n')
            return rho, np.round(avg_k, 4), cata, 'mal'


def expand_reacmat(reactions_matrix, occ):
    """Decomposition of 2-valued rows in stoichiometry matrix into two reactions. (case {A=>B A=>C} and {A=>B C=>B})."""
    rm, new_occ, count, species = np.zeros([1, reactions_matrix.shape[1]]), [], 0, []
    for reac in reactions_matrix:
        if 2 in reac:
            reac_1, reac_2 = np.zeros(reactions_matrix.shape[1]), np.zeros(reactions_matrix.shape[1])
            p, r = np.where(reac == 2)[0], np.where(reac == -2)[0]
            if len(p) > 1:
                reac_1[p[0]], reac_1[r] = 1, -1
                reac_2[p[1]], reac_2[r] = 1, -1
                species.append(p[0]), species.append(p[1])
            else:
                reac_1[r[0]], reac_1[p] = -1, 1
                reac_2[r[1]], reac_2[p] = -1, 1
                species.append(r[0]), species.append(r[1])
            rm = np.vstack((rm, reac_1, reac_2))
            new_occ.append(occ[count]), new_occ.append(occ[count])
        else:
            rm = np.vstack((rm, reac))
            new_occ.append(occ[count])
            reactants, products = np.where(reac == -1)[0], np.where(reac == 1)[0]
            if not reactants.size:
                species.append(products[0])
            elif not products.size:
                species.append(reactants[0])
            else:
                species.append(products[0])
        count += 1
    rm = rm[1:]
    return rm, new_occ, species


def reaction_inference(diff, delta, knowledge, already_inferred):
    """From the transitions return reactions, set of transitions used for the inference and species to target when computing kinetics."""
    reactions_matrix = np.zeros(diff.shape)
    # check there is still non null difference vectors
    for d in range(len(diff)):
        if not diff[d].any():
            continue
        abs_transi = np.abs(diff[d])
        idx_highest_val = np.argmax(abs_transi)
        # select the species that will be linked to the highest one consumed/produced.
        I = np.where(abs_transi[idx_highest_val] <= (1 + delta) * abs_transi)[0]
        # if no species can be matched to highest varying species :
        if I.size <= 1:
            prod, gaps = [], []
            if diff[d][idx_highest_val] < 0:
                other_s = [j for j in range(len(diff[d])) if (j != idx_highest_val) and (diff[d][j] > 0)]
                sgn = -1
            else:
                other_s = [j for j in range(len(diff[d])) if (j != idx_highest_val) and (diff[d][j] < 0)]
                sgn = 1
            for s1, s2 in combinations(other_s, 2):
                gap = np.abs(1 - (- (diff[d][s1] + diff[d][s2])) / diff[d][idx_highest_val])
                if gap < delta:
                    prod.append([s1, s2]), gaps.append(gap)
            if gaps:
                g = np.argmin(gaps)
                # use 2 in the stoichio matrix to distinguish transitions that give two reactions instead of one
                reactions_matrix[d, idx_highest_val], reactions_matrix[d, prod[g]] = sgn * 2, - sgn * 2
            else:
                # both cases failed, species cannot be linked to other species/sum of two species, infer simple degradation/synthesis.
                reactions_matrix[d, idx_highest_val] = sgn
        else:
            r, p = list(I[np.where(diff[d][I] < 0)[0]]), list(I[np.where(diff[d][I] > 0)[0]])
            reactions_matrix[d, r], reactions_matrix[d, p] = -1, 1
    reactions_matrix, indexes, inverse = np.unique(reactions_matrix, return_index=True, return_inverse=True, axis=0)
    # indexes where each reaction was witnessed
    empty_reacs = np.where(~reactions_matrix.any(axis=1))[0]
    reacs = [j for j in range(len(reactions_matrix)) if j not in empty_reacs]
    reactions_matrix = reactions_matrix[reacs]
    occurrences = [[j for j, x in enumerate(inverse) if x == l] for l in reacs]
    reactions_matrix, occurrences, species = expand_reacmat(reactions_matrix, occurrences)
    # prior knowledge handling
    prior_list = []
    if knowledge != []:
        for i in range(len(reactions_matrix)):
            if (knowledge == reactions_matrix[i]).all(axis=1).any():
                prior_list.append(i)
        occurrences = [occurrences[i] for i in range(len(occurrences)) if i not in prior_list]
        species = [species[i] for i in range(len(species)) if i not in prior_list]
        reactions_matrix = np.delete(reactions_matrix, prior_list, axis=0)
    # do not consider reaction already inferred
    already_inferred_list = []
    for i in range(len(reactions_matrix)):
        if (already_inferred == reactions_matrix[i]).all(axis=1).any():
            already_inferred_list.append(i)
    occurrences = [occurrences[i] for i in range(len(occurrences)) if i not in already_inferred_list]
    species = [species[i] for i in range(len(species)) if i not in already_inferred_list]
    reactions_matrix = np.delete(reactions_matrix, already_inferred_list, axis=0)
    return reactions_matrix, occurrences, species


def prior_knowledge(T, prior, header, beta):
    """Take as input a txt file and propagate reactions in the update. Handle A+B=>C+D reactions with MAL so far."""
    if prior == []:
        return T, []
    knowledge = np.zeros(len(header) - 1)
    with open(prior) as f:
        reacs = [line.rstrip('\n') for line in f]
    for reac in reacs:
        z = np.zeros(len(header) - 1)
        sep = [x for x, v in enumerate(reac) if v == ';']
        kine_type = reac[:sep[0]]
        kinetics = float(reac[sep[0] + 1:sep[1]])
        reactants = reac[sep[1] + 1:sep[2]]
        products = reac[sep[2] + 1:sep[3]]
        cata = reac[sep[3] + 1:]
        # get reactants products and cata separated and fill reaction vector
        com = reactants.find('@')
        if com != -1:
            r1, r2 = reactants[:com], reactants[com + 1:]
            idx_r1, idx_r2 = header.index(r1) - 1, header.index(r2) - 1
            z[idx_r1], z[idx_r2] = -1, -1
        elif com == -1 and reactants != '_':
            r1 = reactants
            idx_r1 = header.index(r1) - 1
            z[idx_r1] = -1
        com = products.find('@')
        if com != -1:
            p1, p2 = products[:com], products[com + 1:]
            idx_p1, idx_p2 = header.index(p1) - 1, header.index(p2) - 1
            z[idx_p1], z[idx_p2] = 1, 1
        elif com == -1 and products != '_':
            p1 = products
            idx_p1 = header.index(p1) - 1
            z[idx_p1] = 1
        if cata != '_':
            idx_cata = header.index(cata) - 1
            cata = idx_cata
        else:
            cata = []
        T = transition_update(T, z, kinetics, cata, beta, kine_type)
        knowledge = np.vstack((knowledge, z))
    # cell cycle autocatalysis + stochio reac:
    # z = np.array([0, 0, 0, 1, -1, 0])
    # T = transition_update(T, z, 180, 4, beta, 'stochio')
    knowledge = knowledge[1:, :]
    return T, knowledge


def reaction_learning_loop(D, T, delta, alpha, beta, header, prior=[], micha=False, stochastic=False, verbose=False):
    """Learn CRN (Algo in article)."""
    # case empty T e.g. time horizon too low
    if not T:
        return [], np.zeros([1, len(header) - 1]), [], [], 0
    new_T = copy.deepcopy(T)
    n_species = len(header) - 1
    new_T, knowledge = prior_knowledge(new_T, prior, header, beta)
    new_T = filter_var(new_T, beta)
    best_score, glob_scores = 0, []
    final_cata, final_kinetics, final_matrix = [], [], np.zeros([1, n_species])
    while best_score < alpha:
        diff = np.array([new_T[i][1][1:] for i in range(len(new_T))])  # more practical structure to work on for reaction_inference fct
        reactions_matrix, occurrences, species = reaction_inference(diff, delta, knowledge, final_matrix)
        # case no more reaction to infer
        if len(reactions_matrix) == 0:
            break
        score, glob_kinetics, catalysts = [], [], []
        len_occ, keep = [], [i for i in range(len(reactions_matrix))]
        # keep only the most present reactions
        for occ in occurrences:
            len_occ.append(len(occ))
            keep = np.argsort(len_occ)[-4:]
        kine_types = []
        for idx_reac in keep:
            if verbose:
                reactants, products = np.where(reactions_matrix[idx_reac] == -1)[0], np.where(reactions_matrix[idx_reac] == 1)[0]
                print('considering reaction {} => {}'.format([header[i + 1] for i in reactants], [header[i + 1] for i in products]))
                print('species used for kinetic inference : {}\n{} transitions displaying the reaction'.format(header[species[idx_reac] + 1], len(occurrences[idx_reac])))
            rho, kinetics, cata, kine_type = catalyst_inference(reactions_matrix[idx_reac], new_T, header, alpha, micha, species[idx_reac], stochastic, verbose)
            kine_types.append(kine_type)
            score.append(rho), glob_kinetics.append(kinetics), catalysts.append(cata)
        idx_best = np.nanargmin(score)
        kine, best_score = kine_types[idx_best], score[idx_best]
        if best_score > alpha:
            break
        fiable_reac, fiable_kinetics = reactions_matrix[keep[idx_best]], glob_kinetics[idx_best]
        final_matrix = np.vstack([final_matrix, fiable_reac])
        glob_scores.append(best_score), final_cata.append(catalysts[idx_best]), final_kinetics.append(fiable_kinetics)
        reactants, products = list(np.where(fiable_reac == -1)[0]), list(np.where(fiable_reac == 1)[0])
        print('best reac is {}({}) for {} => {}  with score {}\n \n'.format(kine, fiable_kinetics, [header[i + 1] for i in reactants], [header[i + 1] for i in products], score[idx_best]))
        new_T = transition_update(new_T, fiable_reac, fiable_kinetics, catalysts[idx_best], beta, kine, stochastic=stochastic)
    final_matrix = np.delete(final_matrix, 0, axis=0)
    # temporary hack around empty learned set
    if len(final_matrix) == 0:
        return new_T, np.zeros([2, len(header) - 1]), final_cata, final_kinetics, glob_scores
    return new_T, final_matrix, final_cata, final_kinetics, glob_scores


def transition_update(T, reac, kinetics, cata, beta, kine_type, stochastic=False):
    """Update transitions given a reaction and kinetic parameters."""
    reactants, products = list(np.where(reac == -1)[0]), list(np.where(reac == 1)[0])
    acting_species = reactants.copy()
    # do not update transitions where reactants are absent
    modif_idx = (i for i in range(len(T)) if (np.prod([T[i][0][r + 1] for r in reactants]) > 0))
    if type(cata) is not list:
        acting_species += [cata]
        modif_idx = (i for i in modif_idx if T[i][0][cata + 1])
    for i in modif_idx:
        if kine_type == 'mal':
            if stochastic and len(acting_species) > 1:
                var_update = T[i][1][0] * np.prod([T[i][0][a + 1] for a in acting_species]) * (kinetics / ((len(acting_species) - 1) * 100))
            else:
                var_update = T[i][1][0] * np.prod([T[i][0][a + 1] for a in acting_species]) * kinetics
        # handle cell cycle stochio reac
        elif kine_type == 'cell_stochio':
            var_update = T[i][1][0] * T[i][0][-2] * ((T[i][0][-3])**2) * kinetics
        else:
            var_update = T[i][1][0] * kinetics[0] * (T[i][0][reactants[0] + 1] / (kinetics[1] + T[i][0][reactants[0] + 1]))
        T[i][1][[p + 1 for p in products]] -= var_update
        T[i][1][[r + 1 for r in reactants]] += var_update
    T = filter_var(T, beta)
    return T
