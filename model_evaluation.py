import copy

from data_processing import get_transitions, moving_average, read_multiple_data, subsampling

from inference import reaction_learning_loop

import matplotlib.pyplot as plt
from matplotlib import rc

import numpy as np

rc('text', usetex=True)


def f1_score(reac_truth, reac_inferred, cata_truth, cata_inferred):
    """Compute F1-Score."""
    TP, FP, FN = 0, 0, 0
    TP_idx = []
    if (reac_inferred.size == 0) or (reac_inferred == []):
        return 0
    # stupid data structure
    reac_inferred = np.asmatrix(reac_inferred)
    for i in range(len(reac_inferred)):
        # check if reac was inferred
        idx_reac = np.where((reac_inferred[i] == reac_truth).all(axis=1))[0]
        if idx_reac.size:
            # check if cata match
            if cata_inferred[i] == cata_truth[idx_reac[0]]:
                TP += 1
                TP_idx.append(idx_reac[0])
            else:
                FP += 1
        else:
            FP += 1
    FN_idx = [i for i in range(len(reac_truth)) if i not in TP_idx]
    FN = len(FN_idx)
    precision, recall = TP / (TP + FP), TP / (TP + FN)
    # handle division by zero case
    return 0 if precision + recall == 0 else 2 * (precision * recall) / (precision + recall)


def evaluate_sub(path, time, n_files, sub_list, reac_truth, cata_truth, delta, alpha, beta, discarded, stochastic=False):
    """Evaluate subsampling. <n_files> traces are chosen randomly among the pool of traces and a subsampling step applied.

    Algorithm is then run and the F1_score computed. This is done for the same files, but with different subsampling steps.
    """
    score, rep = [], 11
    pool = [i for i in range(1, 150) if i not in discarded]
    files = np.random.choice(pool, n_files, replace=False)
    with open('test.txt', 'w') as f:
        for i in files:
            if stochastic:
                for j in range(1, rep):
                    print('data/stochastic/{}/{}{}_{}.csv'.format(path, path, i, j), file=f)
            else:
                print('data/ode/{}/{}{}.csv'.format(path, path, i), file=f)
    header, data, discarded = read_multiple_data('test.txt', time)
    if stochastic:
        data = moving_average(data)
    new_sub_list = np.copy(sub_list)
    new_sub_list /= time
    for s in new_sub_list:
        new_data = copy.deepcopy(data)
        print('with files {}, T={}, sub={}, alpha={}'.format(files, time, s, alpha))
        new_data = subsampling(new_data, s)
        T = get_transitions(new_data)
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(new_data, T, delta, alpha, beta, header, micha=0, stochastic=stochastic, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def evaluate_init(path, time, list_n_files, sub, reac_truth, cata_truth, delta, alpha, beta, discarded, stochastic=False):
    """Evaluate initial conditions. The algorithm is run with a trace (X_1) as input, then with traces (X_1, X_2) etc."""
    score, rep = [], 11
    pool = [i for i in range(1, 150) if i not in discarded]
    files = np.random.choice(pool, 1, replace=False)
    for n in range(len(list_n_files) + 1):
        with open('test.txt', 'w') as f:
            for i in files:
                if stochastic:
                    for j in range(1, rep):
                        print('data/stochastic/{}/{}{}_{}.csv'.format(path, path, i, j), file=f)
                else:
                    print('data/ode/{}/{}{}.csv'.format(path, path, i), file=f)
        header, data, discarded = read_multiple_data('test.txt', time)
        if stochastic:
            data = moving_average(data)
        print('with files {}, T={}, sub={}, alpha={}'.format(files, time, sub, alpha))
        T = get_transitions(data)
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(data, T, delta, alpha, beta, header, micha=0, stochastic=stochastic, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
        pool = [i for i in pool if i not in files]
        if n == len(list_n_files):
            return score
        files = np.append(files, np.random.choice(pool, list_n_files[n], replace=False))


def evaluate_time(path, list_time, n_files, sub, reac_truth, cata_truth, delta, alpha, beta, discarded, stochastic=False):
    """Evaluate time horizon. At each iteration the algorithm is applied on the same files but with an increasing time horizon."""
    score, rep = [], 11
    pool = [i for i in range(1, 150) if i not in discarded]
    files = np.random.choice(pool, n_files, replace=False)
    with open('test.txt', 'w') as f:
        for i in files:
            if stochastic:
                for j in range(1, rep):
                    print('data/stochastic/{}/{}{}_{}.csv'.format(path, path, i, j), file=f)
            else:
                print('data/ode/{}/{}{}.csv'.format(path, path, i), file=f)
    for time in list_time:
        print('with files {}, T={}, sub={}, alpha={}'.format(files, time, sub, alpha))
        header, data, discarded = read_multiple_data('test.txt', time)
        if stochastic:
            data = moving_average(data)
        T = get_transitions(data)
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(data, T, delta, alpha, beta, header, micha=0, stochastic=stochastic, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def evaluate_alpha(path, time, n_files, sub, reac_truth, cata_truth, delta, list_alpha, beta, discarded, stochastic=False):
    """Evaluate alpha sensitivity. At each iteration the algorithm is applied on the same files but with an increasing reaction acceptance threshold alpha."""
    score, rep = [], 11
    pool = [i for i in range(1, 150) if i not in discarded]
    files = np.random.choice(pool, n_files, replace=False)
    with open('test.txt', 'w') as f:
        for i in files:
            if stochastic:
                for j in range(1, rep):
                    print('data/stochastic/{}/{}{}_{}.csv'.format(path, path, i, j), file=f)
            else:
                print('data/ode/{}/{}{}.csv'.format(path, path, i), file=f)
    header, data, discarded = read_multiple_data('test.txt', time)
    if stochastic:
        data = moving_average(data)
    T = get_transitions(data)
    for alpha in list_alpha:
        print('with files {}, T={}, sub={}, alpha={}'.format(files, time, sub, alpha))
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(data, T, delta, alpha, beta, header, micha=0, stochastic=stochastic, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def evaluate_beta(path, time, n_files, sub, reac_truth, cata_truth, delta, alpha, list_beta, discarded, stochastic=False):
    """Evaluate beta sensitivity. At each iteration the algorithm is applied on the same files but with an increasing variation filtering threshold beta."""
    score, rep = [], 11
    pool = [i for i in range(1, 150) if i not in discarded]
    files = np.random.choice(pool, n_files, replace=False)
    with open('test.txt', 'w') as f:
        for i in files:
            if stochastic:
                for j in range(1, rep):
                    print('data/stochastic/{}/{}{}_{}.csv'.format(path, path, i, j), file=f)
            else:
                print('data/ode/{}/{}{}.csv'.format(path, path, i), file=f)
    header, data, discarded = read_multiple_data('test.txt', time)
    if stochastic:
        data = moving_average(data)
    T = get_transitions(data)
    for beta in list_beta:
        print('with files {}, T={}, sub={}, beta={}'.format(files, time, sub, beta))
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(data, T, delta, alpha, beta, header, micha=0, stochastic=stochastic, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def evaluate_delta(path, time, n_files, sub, reac_truth, cata_truth, list_delta, alpha, beta, discarded, stochastic=False):
    """Evaluate delta sensitivity. At each iteration the algorithm is applied on the same files but with an increasing species binding threshold delta."""
    score, rep = [], 11
    pool = [i for i in range(1, 150) if i not in discarded]
    files = np.random.choice(pool, n_files, replace=False)
    with open('test.txt', 'w') as f:
        for i in files:
            if stochastic:
                for j in range(1, rep):
                    print('data/stochastic/{}/{}{}_{}.csv'.format(path, path, i, j), file=f)
            else:
                print('data/ode/{}/{}{}.csv'.format(path, path, i), file=f)
    header, data, discarded = read_multiple_data('test.txt', time)
    if stochastic:
        data = moving_average(data)
    T = get_transitions(data)
    for delta in list_delta:
        print('with files {}, T={}, sub={}, delta={}'.format(files, time, sub, delta))
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(data, T, delta, alpha, beta, header, micha=0, stochastic=stochastic, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def eval_sub_cano(path, time, sub_list, reac_truth, cata_truth, delta, alpha, beta, stochastic=False):
    """Evaluate subsampling for canonical trace."""
    score, rep = [], 11
    with open('test.txt', 'w') as f:
        if stochastic:
            for j in range(1, rep):
                print('data/stochastic/{}/cano_{}{}.csv'.format(path, path, j), file=f)
        else:
            print('data/ode/{}/cano_{}.csv'.format(path, path), file=f)
    header, data, discarded = read_multiple_data('test.txt', time)
    if stochastic:
        data = moving_average(data)
    new_sub_list = np.copy(sub_list)
    new_sub_list /= time
    for s in new_sub_list:
        new_data = copy.deepcopy(data)
        new_data = subsampling(new_data, s)
        T = get_transitions(new_data)
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(new_data, T, delta, alpha, beta, header, micha=0, stochastic=stochastic, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def eval_time_cano(path, list_time, sub, reac_truth, cata_truth, delta, alpha, beta, stochastic=False):
    """Evaluate time horizon for canonical trace."""
    score, rep = [], 11
    with open('test.txt', 'w') as f:
        if stochastic:
            for j in range(1, rep):
                print('data/stochastic/{}/cano_{}{}.csv'.format(path, path, j), file=f)
        else:
            print('data/ode/{}/cano_{}.csv'.format(path, path), file=f)
    for time in list_time:
        header, data, discarded = read_multiple_data('test.txt', time)
        if stochastic:
            data = moving_average(data)
        T = get_transitions(data)
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(data, T, delta, alpha, beta, header, micha=0, stochastic=stochastic, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def eval_alpha_cano(path, time, sub, reac_truth, cata_truth, delta, list_alpha, beta, stochastic=False):
    """Evaluate alpha for canonical trace."""
    score, rep = [], 11
    with open('test.txt', 'w') as f:
        if stochastic:
            for j in range(1, rep):
                print('data/stochastic/{}/cano_{}{}.csv'.format(path, path, j), file=f)
        else:
            print('data/ode/{}/cano_{}.csv'.format(path, path), file=f)
    header, data, discarded = read_multiple_data('test.txt', time)
    if stochastic:
        data = moving_average(data)
    T = get_transitions(data)
    for alpha in list_alpha:
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(data, T, delta, alpha, beta, header, micha=0, stochastic=stochastic, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def eval_beta_cano(path, time, sub, reac_truth, cata_truth, delta, alpha, list_beta, stochastic=False):
    """Evaluate beta for canonical trace."""
    score, rep = [], 11
    with open('test.txt', 'w') as f:
        if stochastic:
            for j in range(1, rep):
                print('data/stochastic/{}/cano_{}{}.csv'.format(path, path, j), file=f)
        else:
            print('data/ode/{}/cano_{}.csv'.format(path, path), file=f)
    header, data, discarded = read_multiple_data('test.txt', time)
    if stochastic:
        data = moving_average(data)
    T = get_transitions(data)
    for beta in list_beta:
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(data, T, delta, alpha, beta, header, micha=0, stochastic=stochastic, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def eval_delta_cano(path, time, sub, reac_truth, cata_truth, list_delta, alpha, beta, stochastic=False):
    """Evaluate delta for canonical trace."""
    score, rep = [], 11
    with open('test.txt', 'w') as f:
        if stochastic:
            for j in range(1, rep):
                print('data/stochastic/{}/cano_{}{}.csv'.format(path, path, j), file=f)
        else:
            print('data/ode/{}/cano_{}.csv'.format(path, path), file=f)
    header, data, discarded = read_multiple_data('test.txt', time)
    if stochastic:
        data = moving_average(data)
    T = get_transitions(data)
    for delta in list_delta:
        new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(data, T, delta, alpha, beta, header, micha=0, stochastic=stochastic, verbose=False)
        score.append(f1_score(reac_truth, final_matrix, cata_truth, final_cata))
    return score


def eval_init_cano(path, time, sub, reac_truth, cata_truth, delta, alpha, beta, stochastic=False):
    """Evaluate alpha for canonical trace."""
    rep = 11
    with open('test.txt', 'w') as f:
        if stochastic:
            for j in range(1, rep):
                print('data/stochastic/{}/cano_{}{}.csv'.format(path, path, j), file=f)
        else:
            print('data/ode/{}/cano_{}.csv'.format(path, path), file=f)
    header, data, discarded = read_multiple_data('test.txt', time)
    if stochastic:
        data = moving_average(data)
    T = get_transitions(data)
    new_diff, final_matrix, final_cata, final_mal, glob_scores = reaction_learning_loop(data, T, delta, alpha, beta, header, micha=0, stochastic=stochastic, verbose=False)
    return f1_score(reac_truth, final_matrix, cata_truth, final_cata)


def truth_model(model):
    """Return model stoichioemetry matrix, catalysts and traces containing simulation errors / stuck in attractors for selected model.

    Traces containing simulation errors will be ignored in the random sampling in the evalution process.
    Errors can be:
    - several time points w/ time "0" (due to numerical integration failure)
    - species stuck in attractor at the beginning of the trace.
    """
    if model == 'ode_parallel':
        true_cata = [[2], [], []]
        true_matrix = np.zeros([3, 6])
        true_matrix[0] = [0, 0, 0, 0, 1, -1]
        true_matrix[1] = [0, 0, 1, -1, 0, 0]
        true_matrix[2] = [1, -1, 0, 0, 0, 0]
        d = [2, 31, 58, 68, 81, 95, 97, 105, 111, 148]
        return true_cata, true_matrix, d

    if model == 'ode_product_parallel':
        true_cata = [[0], [], []]
        true_matrix = np.zeros([3, 5])
        true_matrix[0] = [0, 0, 0, 1, -1]
        true_matrix[1] = [0, -1, 1, 0, 0]
        true_matrix[2] = [1, -1, 0, 0, 0]
        d = [35, 44, 79]
        return true_cata, true_matrix, d

    if model == 'ode_reactant_parallel':
        true_cata = [[], [], []]
        true_matrix = np.zeros([3, 6])
        true_matrix[0] = [0, 0, 0, 1, -1, -1]
        true_matrix[1] = [1, -1, 0, 0, 0, 0]
        true_matrix[2] = [1, 0, -1, 0, 0, 0]
        d = [4, 17, 25, 27, 30, 58, 73, 75, 95, 132]
        return true_cata, true_matrix, d

    if model == 'ode_chain':
        true_cata = [[], [], [], []]
        true_matrix = np.zeros([4, 5])
        true_matrix[0] = [0, 0, 0, 1, -1]
        true_matrix[1] = [0, 0, 1, -1, 0]
        true_matrix[2] = [0, 1, -1, 0, 0]
        true_matrix[3] = [1, -1, 0, 0, 0]
        d = [42, 52, 67, 81, 89, 142]
        return true_cata, true_matrix, d

    if model == 'sto_parallel':
        true_matrix = np.zeros([3, 6])
        true_matrix[0] = [0, 0, 0, 0, -1, 1]
        true_matrix[1] = [0, 0, -1, 1, 0, 0]
        true_matrix[2] = [-1, 1, 0, 0, 0, 0]
        true_cata = [[], [], [3]]
        d = [67, 91, 112, 142, 145]
        return true_cata, true_matrix, d

    if model == 'sto_product_parallel':
        true_matrix = np.zeros([3, 5])
        true_matrix[0] = [0, 0, -1, 0, 1]
        true_matrix[1] = [0, 0, -1, 1, 0]
        true_matrix[2] = [-1, 1, 0, 0, 0]
        true_cata = [[], [], [3]]
        d = [2, 9, 11, 15, 17, 20, 21, 23, 31, 49, 61, 69, 72, 82, 89, 96, 99, 102, 115, 118, 120, 122, 127, 134, 148]
        return true_cata, true_matrix, d

    if model == 'sto_reactant_parallel':
        true_cata = [[], [], []]
        true_matrix = np.zeros([3, 6])
        true_matrix[0] = [-1, 1, 0, 0, 0, -1]
        true_matrix[1] = [0, 0, -1, 1, 0, 0]
        true_matrix[2] = [0, 0, 0, 1, -1, 0]
        d = [15, 62, 91, 146]
        return true_cata, true_matrix, d

    if model == 'sto_chain':
        true_cata = [[], [], [], []]
        true_matrix = np.zeros([4, 5])
        true_matrix[0] = [0, 0, 0, -1, 1]
        true_matrix[1] = [0, 0, -1, 1, 0]
        true_matrix[2] = [0, -1, 1, 0, 0]
        true_matrix[3] = [-1, 1, 0, 0, 0]
        d = []
        return true_cata, true_matrix, d


def plot_generator_params(list_alpha, list_beta, list_delta, mean_alpha, mean_beta,
                          mean_delta, std_alpha, std_beta, std_delta,
                          score_alpha_one, score_beta_one, score_delta_one, output):
    """Save parameters sensitivity results in "<output>_parameters.eps"."""
    fig, axes = plt.subplots(1, 3, figsize=(17, 5))
    fig.text(0.07, 0.5, r'$F_1$ Score', ha='center', va='center', rotation='vertical')
    axes[0].set_xlabel(r'variation filtering threshold $\beta$')
    axes[1].set_xlabel(r'species binding slack threshold $\delta$')
    axes[2].set_xlabel(r'reaction acceptance threshold $\alpha$')
    [axes[i].set_ylim(0, 1.1) for i in range(3)]
    axes[0].errorbar(list_beta, mean_beta, yerr=std_beta, marker='o', linestyle='--')
    axes[0].plot(list_beta, score_beta_one[0], marker='o', linestyle='--', color='r')
    axes[1].errorbar(list_delta, mean_delta, yerr=std_delta, marker='o', linestyle='--')
    axes[1].plot(list_delta, score_delta_one[0], marker='o', linestyle='--', color='r')
    axes[2].errorbar(list_alpha, mean_alpha, yerr=std_alpha, marker='o', linestyle='--')
    axes[2].plot(list_alpha, score_alpha_one[0], marker='o', linestyle='--', color='r')
    fig.savefig('{}_parameters.eps'.format(output), transparent=True, dpi=900)


def plot_generator_ci(list_sub, list_time, list_init, mean_sub,
                      mean_time, mean_init, std_sub, std_time, std_init,
                      score_sub_one, score_time_one, score_init_one, output):
    """Save initial conditions sensitivity results in "<output>_ci.eps"."""
    fig, axes = plt.subplots(1, 3, figsize=(17, 5))
    sinit = [score_init_one for i in range(len(list_init))]
    fig.text(0.07, 0.5, r'$F_1$ Score', ha='center', va='center', rotation='vertical')
    axes[0].set_xlabel('timelapse as \% of the time horizon')
    axes[1].set_xlabel('time horizon')
    axes[2].set_xlabel('number of traces included')
    axes[2].set_xlim(0, 16)
    [axes[i].set_ylim(0, 1.1) for i in range(3)]
    axes[0].errorbar(list_sub, mean_sub, yerr=std_sub, marker='o', linestyle='--')
    axes[0].plot(list_sub, score_sub_one[0], marker='o', linestyle='--', color='r')
    axes[1].errorbar(list_time, mean_time, yerr=std_time, marker='o', linestyle='--')
    axes[1].plot(list_time, score_time_one[0], marker='o', linestyle='--', color='r')
    axes[2].errorbar(list_init, mean_init, yerr=std_init, marker='o', linestyle='--')
    axes[2].plot(list_init, sinit, marker='o', linestyle='--', color='r')
    fig.savefig('{}_ci.eps'.format(output), transparent=True, dpi=900)
