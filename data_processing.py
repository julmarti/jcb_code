import copy
import csv
from typing import List, Tuple

import numpy as np

Header = List[str]
Point = List[float]
Series = List[Point]
Data = List[Series]
Transitions = List[Tuple[List, np.array, int]]


def read_multiple_data(files_list: str, time_horizon=np.inf) -> Tuple[Header, Data, List]:
    """Read a list of biocham simulation data files from CSV files and return it."""
    header: Header = []
    data: Data = []
    idx_discarded = []
    with open(files_list) as f:
        files = [line.rstrip('\n') for line in f]
    for file in files:
        time_series: Series = []
        header = []
        with open(file, "r", newline="") as f:
            for row in csv.reader(f):
                if header == []:
                    # remove first '#' character
                    row[0] = row[0][1:]
                    header = row
                else:
                    time_series.append(list(map(float, row)))
        # discard traces with no species at the beginning as well as no synthesis
        if ((sum(time_series[0][1:]) == sum(time_series[1][1:])) and (sum(time_series[0][1:]) == 0)):
            idx_discarded.append(file)
            continue
        # discard traces stuck in attractor at beginning or w/ several time points where t = 0 (numerical issues)
        if len(time_series) == 2 or (time_series[1][0] == 0):
            idx_discarded.append(file)
            continue
        idx_time_horizon = np.searchsorted([time_series[i][0] for i in range(len(time_series))], time_horizon, side='right')
        data.append(time_series[0:idx_time_horizon])
    return header, data, idx_discarded


def subsampling(data: Data, step: float) -> Data:
    """Take list of lists of biocham csv files and return subsampled data."""
    sub_data: Data = []
    if not step:
        return data
    for trace in range(len(data)):
        steps = [x * step for x in range(np.int((1 / step) * data[trace][-1][0]) + 1) if x * step < data[trace][-1][0] + 0.01]
        idx_sub = np.unique(np.searchsorted([data[trace][i][0] for i in range(0, len(data[trace]) - 1)], steps, side="left"))
        sub_data.append([data[trace][i] for i in idx_sub])
    return sub_data


def moving_average(data: Data, n=5) -> Data:
    """Smooth stochastic curve. Stable for n=5 so far."""
    if data == []:
        return data
    n_var = len(data[0][0])
    new_data: Data = []
    for i in range(len(data)):
        if len(data[i]) < n:
            new_data.append(data[i])
            continue
        d = [data[i][0]]
        add = np.empty([len(data[i]) - 4, 0])
        for j in range(n_var):
            ret = np.cumsum([data[i][l][j] for l in range(len(data[i]))], dtype=float)
            ret[n:] = ret[n:] - ret[:-n]
            ret = ret[n - 1:] / n
            ret = np.reshape(ret, (len(ret), 1))
            add = np.hstack((add, ret))
        for m in range(len(ret)):
            d.append(list(add[m, :]))
        new_data.append(d)
    return new_data


def get_transitions(data: Data) -> Transitions:
    """Compute transitions set T = {(state_vector, diff_vector, trace_idx)}."""
    T: Transitions = []
    for l in range(len(data)):
        T += [(data[l][i], np.diff(data[l], axis=0)[i], l) for i in range(len(data[l]) - 1)]
    return T


def filter_var(T, beta) -> Tuple[Series, list]:
    """Remove insignificant variations in a trace w.r.t. the vector of stastical maxima of this trace."""
    T_final, T_dt = copy.deepcopy(T), copy.deepcopy(T)
    n_files, n_species = T[-1][-1] + 1, len(T[0][0])
    M = np.zeros([n_files, n_species])
    # dx/dt
    for i in range(len(T)):
        T_dt[i][1][1:] = np.abs(T_dt[i][1][1:]) / T[i][1][0]
    # fill matrix w/ statistical maximum file per file
    file_provenance = [T[i][-1] for i in range(len(T))]
    dump, interval = np.unique(file_provenance, return_index=True)
    for i in range(len(interval)):
        T_dt_file = T_dt[interval[i]:interval[i + 1]] if i < len(interval) - 1 else T_dt[interval[i]:]
        diff = np.array([T_dt_file[j][1] for j in range(len(T_dt_file))])
        M[i, :] = np.mean(diff, axis=0) + np.std(diff, axis=0)
    # test for every transitions
    for i in range(len(T)):
        T_final[i][1][1:][T_dt[i][1][1:] < beta * M[T[i][-1], 1:]] = 0
    return T_final


def print_model(reaction_matrix, catalysts, kinetics, header):
    """Print learned reaction set."""
    for reac, kine, cata in zip(reaction_matrix, kinetics, catalysts):
        R = np.where(reac == -1)[0]
        P = np.where(reac == 1)[0]
        R_name = [header[i + 1] for i in R]
        P_name = [header[i + 1] for i in P]
        if type(kine) != list:
            if type(cata) == list:
                print('{} => {} MA({})'.format(R_name, P_name, kine))
            else:
                print('{} => {} MA({}) with cata {}'.format(R_name, P_name, kine, header[cata + 1]))
        else:
            print('{} => {} MM({})'.format(R_name, P_name, kine))
