import sys

from model_evaluation import eval_alpha_cano, eval_beta_cano, eval_delta_cano, eval_init_cano, eval_sub_cano, eval_time_cano, evaluate_alpha, evaluate_beta, evaluate_delta, evaluate_init, evaluate_sub, evaluate_time, plot_generator_ci, plot_generator_params, truth_model

import numpy as np


# settings for sensitivity analysis

np.random.seed(20)

list_init = [1, 2, 3, 4, 5, 7, 10, 15]
list_sub = np.linspace(0, 10, 11)
list_alpha = np.linspace(0.1, 2, 10)
list_delta = np.linspace(0.1, 2, 10)
list_beta = np.linspace(0, .2, 11)[1:]
list_time = [0.01, 0.1, 0.5, 1, 5, 10, 15]
n_rep, nfiles, sub, time_horizon = 25, 5, 0, 10
stochastic = False
if stochastic:
    beta, alpha, delta = .1, 1, 1
    kind_truth = 'sto'
else:
    beta, alpha, delta = .1, .4, 1
    kind_truth = 'ode'

### PARALLEL CASE ###

paths = ['parallel', 'reactant_parallel', 'product_parallel', 'chain']

for path in paths:
    score_ini, score_alpha, score_time, score_sub, score_delta, score_beta = [], [], [], [], [], []
    score_sub_one, score_alpha_one, score_time_one, score_delta_one, score_beta_one = [], [], [], [], []
    true_cata, true_matrix, discarded = truth_model('{}_{}'.format(kind_truth, path))

    # run sensitivity analysis for canonical trace
    score_time_one.append(eval_time_cano(path, list_time, sub, true_matrix, true_cata, delta, alpha, beta, stochastic))
    score_sub_one.append(eval_sub_cano(path, time_horizon, list_sub, true_matrix, true_cata, delta, alpha, beta, stochastic))
    score_alpha_one.append(eval_alpha_cano(path, time_horizon, sub, true_matrix, true_cata, delta, list_alpha, beta, stochastic))
    score_delta_one.append(eval_delta_cano(path, time_horizon, sub, true_matrix, true_cata, list_delta, alpha, beta, stochastic))
    score_beta_one.append(eval_beta_cano(path, time_horizon, sub, true_matrix, true_cata, delta, alpha, list_beta, stochastic))
    score_init_one = eval_init_cano(path, time_horizon, sub, true_matrix, true_cata, delta, alpha, beta, stochastic)

    # run sensitivity analysis for multiple traces
    for rep in range(n_rep):
        score_ini.append(evaluate_init(path, time_horizon, np.diff(list_init), sub, true_matrix, true_cata, delta, alpha, beta, discarded, stochastic))
        score_time.append(evaluate_time(path, list_time, nfiles, sub, true_matrix, true_cata, delta, alpha, beta, discarded, stochastic))
        score_sub.append(evaluate_sub(path, time_horizon, nfiles, list_sub, true_matrix, true_cata, delta, alpha, beta, discarded, stochastic))
        score_alpha.append(evaluate_alpha(path, time_horizon, nfiles, sub, true_matrix, true_cata, delta, list_alpha, beta, discarded, stochastic))
        score_beta.append(evaluate_beta(path, time_horizon, nfiles, sub, true_matrix, true_cata, delta, alpha, list_beta, discarded, stochastic))
        score_delta.append(evaluate_delta(path, time_horizon, nfiles, sub, true_matrix, true_cata, list_delta, alpha, beta, discarded, stochastic))

    mean_ini, mean_time, mean_sub, mean_alpha, mean_beta, mean_delta = np.mean(np.array(score_ini), axis=0), np.mean(np.array(score_time), axis=0), np.mean(np.array(score_sub), axis=0), np.mean(np.array(score_alpha), axis=0), np.mean(np.array(score_beta), axis=0), np.mean(np.array(score_delta), axis=0)
    std_ini, std_time, std_sub, std_alpha, std_beta, std_delta = np.std(np.array(score_ini), axis=0), np.std(np.array(score_time), axis=0), np.std(np.array(score_sub), axis=0), np.std(np.array(score_alpha), axis=0), np.std(np.array(score_beta), axis=0), np.std(np.array(score_delta), axis=0)

    plot_generator_ci(list_sub, list_time, list_init, mean_sub,
                      mean_time, mean_ini, std_sub, std_time, std_ini,
                      score_sub_one, score_time_one, score_init_one, '{}_sensitivity_{}'.format(path, kind_truth))

    plot_generator_params(list_alpha, list_beta, list_delta, mean_alpha, mean_beta,
                          mean_delta, std_alpha, std_beta, std_delta,
                          score_alpha_one, score_beta_one, score_delta_one, '{}_sensitivity_{}'.format(path, kind_truth))
sys.exit()
